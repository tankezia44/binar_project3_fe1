import React, { useState, useEffect } from 'react';
import data from '../data/data.json';
import { Button, Checkbox, IconButton, List, ListItem, ListItemText } from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';

function Main() {
    
    //Mengambil dan menampung data dari data.json
    const [dataJson, setdataJson] = useState([]);
    useEffect(() => {
        setdataJson(data);
    }, []);

    //Fitur filter "ALL"
    const handleFilterALL = () => {
        setdataJson(data);
    };

    //Fitur filter "DONE"
    const handleFilterDONE = () => {
        const done = dataJson.filter((item) => item.complete === true);
        setdataJson(done);
    };

    //Fitur filter "TODO"
    const handleFilterTODO = () => {
        const todo = dataJson.filter((item) => item.complete === false);
        setdataJson(todo);
    };
    
    // Menghapus data saat klik icon trash
    const handleDelete = (id) => {
        const newData = dataJson.filter((item) => item.id !== id);
        setdataJson(newData);
    };

  return (
    
    <div>

        <h1 id='judul_filter'>To Do Filter</h1>
      
        <div className='todo_filter'>
            <Button variant="contained" onClick={() => handleFilterALL()} id='filterall'>ALL</Button>
            <Button variant="contained" onClick={() => handleFilterDONE()} id='filterdone'>DONE</Button>
            <Button variant="contained" onClick={() => handleFilterTODO()} id='filtertodo'>TO DO</Button>
        </div>

        <List id='list'>
                {dataJson.map((item) => (
                    <ListItem key={item.id} id='listitem'>
                        <ListItemText primary={`${item.task}`}/>
                        <Checkbox
                        checked={item.complete}
                        />
                        <IconButton> <Edit /> </IconButton>
                        <IconButton>
                            <Delete onClick={() => handleDelete(item.id)}></Delete>
                        </IconButton>
                    </ListItem>
                ))}
        </List>
    </div>
  )
}

export default Main