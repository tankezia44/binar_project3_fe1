import { useEffect, useState } from "react";
import SearchFilter from "react-filter-search";
import "bootstrap/dist/css/bootstrap.min.css"; //Pakai bootstrap khusus react

import "./App.css";
import ButtonTambah from "./components/ButtonTambah.jsx";
import ButtonEdit from "./components/ButtonEdit.jsx";
import ListTodo from "./components/ListTodo.jsx";

function App() {
  // Inisialisasi data json
  const [toDo, setTodo] = useState([
    {
      id: 1,
      task: "Nyuci mobil",
      complete: false,
    },
    {
      id: 2,
      task: "Memberi makan kucing",
      complete: false,
    },
    {
      id: 3,
      task: "Olahraga 10 menit",
      complete: false,
    },
    {
      id: 4,
      task: "Sarapan sereal",
      complete: false,
    },
    {
      id: 5,
      task: "Belanja harian",
      complete: false,
    },
  ]);

  // Pattern/ruang untuk menyimpan data
  // const itu variabel
  // di dalam array itu object
  // hooks --> useState 
  const [newTodo, setNewTodo] = useState("");
  const [updateTodo, setUpdateTodo] = useState("");
  const [searchTodo, setSearchTodo] = useState("");

  // cari data secara langsung --> cek console
  useEffect(() => {
    console.log(searchTodo);
  }, [searchTodo]);

  // tambah data
  const tambah = () => {
    if (newTodo) {
      let num = toDo.length + 1;
      let newEntry = {
        id: num,
        task: newTodo,
        status: false,
      };
      // ...todo --> memasukkan ke valuenya todo, newEntry itu yg akan dimasukkan ke todonya
      setTodo([...toDo, newEntry]);
      setNewTodo(""); // untuk menghilangkan inputan setelah disubmit
    }
  };

  // hapus data
  const hapus = (id) => {
    let newTodos = toDo.filter((task) => task.id !== id);
    setTodo(newTodos);
  };

  const filter = () => {
    let filter = toDo.filter((complete) => complete.id === true);
    console.log(filter);
  };

  // mengecek todo
  const selesai = (id) => {
    let newTodos = toDo.map((task) => {
      if (task.id === id) {
        return { ...task, complete: !task.complete };
      }
      return task;
    });
    setTodo(newTodos);
  };

  // cancle update
  const cancelUpdate = () => {
    setUpdateTodo("");
  };

  // update data
  const ubahData = (e) => {
    let newTodo = {
      id: updateTodo.id,
      task: e.target.value,
      complete: updateTodo.complete ? true : false,
    };
    setUpdateTodo(newTodo);
  };

  // update data
  const updateData = () => {
    // console.log("diklik");
    let filter = [...toDo].filter((task) => task.id !== updateTodo.id);
    let updateObject = [...filter, updateTodo];
    setTodo(updateObject);
    setUpdateTodo("");
  };

  // handle checkbox delete
  // const [isChecked, setisChecked] = useState([]);
  // const handleCheckbox = (e) => {
  //   const { value, checked } = e.target;
  //   // console.log(value);

  //   if (checked) {
  //     setisChecked([...isChecked, value]);
  //   } else {
  //     setisChecked(isChecked.filter((e) => e !== value));
  //   }
  // };

  // const deleteCheckbox = () => {
  //   console.log(isChecked);
  // };

  return (
    <div className="App container">
      <h1 className="mt-5 title">Todo App Kezia</h1>

      {/* search */}
      <div className="row mb-2">
        <div className="col-md-2">
          <input
            className="form-control form-control-lg"
            placeholder="Search..."
            value={searchTodo}
            onChange={(e) => setSearchTodo(e.target.value)}
          />
        </div>
      </div>

      {updateTodo && updateTodo ? (
        <ButtonEdit
          updateTodo={updateTodo}
          ubahData={ubahData}
          updateData={updateData}
          cancelUpdate={cancelUpdate}
        />
      ) : (
        <ButtonTambah
          setNewTodo={setNewTodo}
          tambah={tambah}
          newTodo={newTodo}
        />
      )}

      {/* tampil data */}
      {toDo && toDo.length ? (
        <SearchFilter
          value={searchTodo}
          data={toDo}
          renderResults={(toDo) => (
            <ListTodo
              selesai={selesai}
              toDo={toDo}
              setUpdateTodo={setUpdateTodo}
              hapus={hapus}
              setTodo={setTodo}
              // handleCheckbox={handleCheckbox}
              // deleteCheckbox={deleteCheckbox}
            />
          )}
        />
      ) : (
        <h2 style={{ color: "black" }}>Tidak ada data</h2>
      )}
    </div>
  );
}

export default App;