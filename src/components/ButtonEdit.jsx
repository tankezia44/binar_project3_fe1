function ButtonEdit({ updateTodo, ubahData, updateData, cancelUpdate }) {
    return (
        <>
            <div className="row mb-2">
                <div className="col">
                    <input
                        className="form-control form-control-lg"
                        value={updateTodo && updateTodo.task}
                        onChange={(e) => ubahData(e)}
                    />
                </div>
                <div className="col-auto">
                    <button
                        className="btn btn-lg btn-primary mr-20"
                        onClick={updateData}
                    >
                        Edit Task
                    </button>
                    <button className="btn btn-lg btn-warning" onClick={cancelUpdate}>
                        Cancel
                    </button>
                </div>
            </div>
        </>
    )
}

export default ButtonEdit