function ButtonTambah({ setNewTodo, tambah, newTodo }) {
    return (
      <>
        {/* tambah data */}
        <div className="row mb-2">
          <div className="col">
            <input
              placeholder="Input/Edit Tasks Here..."
              value={newTodo}
              onChange={(e) => setNewTodo(e.target.value)}
              className="form-control form-control-lg"
            />
          </div>
          <div className="col-auto">
            <button className="btn btn-lg btn-primary" onClick={tambah}>
              Tambah Task
            </button>
          </div>
        </div>
      </>
    )
  }
  
  export default ButtonTambah;