import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCircleCheck,
    faPen,
    faTrash,
} from "@fortawesome/free-solid-svg-icons";

function ListTodo({ selesai, toDo, setUpdateTodo, hapus, setTodo, handleCheckbox, deleteCheckbox }) {
    return (
        <>
            {/* <button className="btn btn-danger" onClick={deleteCheckbox}>Delete Via Checkbox</button> */}
            <div className="filterItem">
                <ul>
                    <li>
                        <button>All</button>
                    </li>
                    <li>
                        <button>Complete</button>
                    </li>
                    <li>
                        <button>Todo</button>
                    </li>
                </ul>
            </div>
            {toDo &&
                toDo.map((task, index) => {
                    return (
                        <React.Fragment key={task.id}>
                            <div className="col taskBg">
                                <div className={task.complete ? "done" : ""}>
                                    {/* <input type="checkbox" value={task.id} checked={task.isChecked} onChange={(e) => handleCheckbox(e)} /> */}
                                    <span className="taskText">{task.task}</span>
                                </div>
                                <div className="iconsWrap">
                                    <span onClick={() => selesai(task.id)}>
                                        <FontAwesomeIcon icon={faCircleCheck} />
                                    </span>

                                    {task.complete ? null : (
                                        <span
                                            onClick={() =>
                                                setUpdateTodo({
                                                    id: task.id,
                                                    task: task.task,
                                                    status: task.complete ? true : false,
                                                })
                                            }
                                        >
                                            <FontAwesomeIcon icon={faPen} />
                                        </span>
                                    )}

                                    <span onClick={() => hapus(task.id)}>
                                        <FontAwesomeIcon icon={faTrash} />
                                    </span>
                                </div>
                            </div>
                        </React.Fragment>
                    );
                })}

            {/* delete all */}
            <div className='mb-4'>
                <button className='delete-all btn btn-danger' onClick={() => setTodo([])}>Delete All Tasks</button>
            </div>
        </>
    )
}

export default ListTodo